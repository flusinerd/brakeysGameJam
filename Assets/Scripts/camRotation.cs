﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camRotation : MonoBehaviour
{

    public Transform target;
    public Camera cam;

    private Vector3 offset = new Vector3(0, 3, -7);

    Ray ray;

    void Update()
    {

        ray = cam.ScreenPointToRay(Input.mousePosition);

        followPlayer();

        if (Physics.Raycast(ray))
        {
            cam.transform.LookAt(target.position + ray.direction * 5);
        }

    }

    void followPlayer()
    {
        Vector3 targetPos = target.position + target.forward * offset.z + target.up * offset.y + target.right * offset.x;
        transform.position = Vector3.Lerp(transform.position, targetPos, 10 * Time.deltaTime);
    }

}
