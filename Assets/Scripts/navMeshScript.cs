﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI; //Import Navmesh Namespace    

public class navMeshScript : MonoBehaviour {

    public Transform goal;
    NavMeshAgent Agent;

	// Use this for initialization
	void Start () {
        Agent = GetComponent<NavMeshAgent>(); //Get the Agent component
        Agent.destination = goal.position;  //set the destination to the goals position
	}
}
