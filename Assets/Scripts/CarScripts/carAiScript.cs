﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class carAiScript : MonoBehaviour {
    public Transform path;
    private List<Transform> wayPoints;
    private int currentWaypoint;
    public float maxSteerAngle = 40f;
    public WheelCollider wheelFL, wheelFR, wheelBL, wheelBR;
    float newSteer;
    public bool isBreaking;
    private float targetSteer = 0;
    public float turnAngle, breakDistanceFactor, breakDistance;

    //Performance
    public float maxTorque = 80f;
    public float currentSpeed;
    public float maxSpeed = 100;
    public float breakingAngle = 15, breakForce, steerSmoothing;

    private void Start()
    {
        Transform[] waypointTransforms = path.GetComponentsInChildren<Transform>();
        wayPoints = new List<Transform>();
        for (int i = 0; i < waypointTransforms.Length; i++)
        {
            //Check if not the ownTransform
            if (waypointTransforms[i] != path.transform)
            {
                //Add to List
                wayPoints.Add(waypointTransforms[i]);
            }
        }
    }

    private void FixedUpdate()
    {
        applyPower();
        applySteer();
        checkWaypointDistance();
        smoothSteering();
        calcAngle();
    }

    private void applySteer()
    {
        Vector3 relativeVector = transform.InverseTransformPoint(wayPoints[currentWaypoint].position); //Get relative Vector to next 
        targetSteer = (relativeVector.x / relativeVector.magnitude) * maxSteerAngle;
    }

    private void applyPower()
    {
        currentSpeed = 2 * Mathf.PI * wheelFL.radius * wheelFL.rpm * 60 / 1000;
        if (currentSpeed < maxSpeed && !isBreaking)
        {
            wheelFL.motorTorque = maxTorque;
            wheelFR.motorTorque = maxTorque;
        }
    }

    private void checkWaypointDistance()
    {
        if (Vector3.Distance(transform.position, wayPoints[currentWaypoint].position) < 0.5f)
        {
            if (currentWaypoint == wayPoints.Count - 1) {
                currentWaypoint = 0;
            } else { 
                currentWaypoint++;
            }
        }
    }

    private void Breaking()
    {
        //Change textures here
        if (isBreaking) { 
            wheelBL.brakeTorque = breakForce;
            wheelBR.brakeTorque = breakForce;
            wheelFL.brakeTorque = breakForce/2;
            wheelFR.brakeTorque = breakForce/2;
            Debug.Log("breaking");
        } else
        {
            wheelBL.brakeTorque = 0;
            wheelBR.brakeTorque = 0;
            wheelFL.brakeTorque = 0;
            wheelFR.brakeTorque = 0;
        }
    }

    private void smoothSteering()
    {
        wheelFL.steerAngle = Mathf.Lerp(wheelFL.steerAngle, targetSteer, Time.deltaTime * steerSmoothing);
        wheelFR.steerAngle = Mathf.Lerp(wheelFR.steerAngle, targetSteer, Time.deltaTime * steerSmoothing);
    }

    private void calcAngle()
    {
        int nextWaypoint;
        int prevWaypoint;
        if (currentWaypoint == wayPoints.Count - 1)
        {
            nextWaypoint = 0;
        } else
        {
            nextWaypoint = currentWaypoint + 1;
        };
        if (currentWaypoint == 0)
        {
            prevWaypoint = wayPoints.Count - 1;
        } else
        {
            prevWaypoint = currentWaypoint - 1;
        }
        Vector3 vectorCurrent, vectorNext;
        vectorCurrent = wayPoints[prevWaypoint].position - wayPoints[currentWaypoint].position;
        vectorNext = wayPoints[currentWaypoint].position - wayPoints[nextWaypoint].position;
        turnAngle = Vector3.Angle(vectorCurrent, vectorNext);
    }
}
