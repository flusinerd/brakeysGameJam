﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarMovement : MonoBehaviour {

    public float speed = 100f, steerSpeed = 4f;

    public WheelCollider WheelRightUp, WheelRightDown, WheelLeftUp, WheelLeftDown;
    public Transform WheelRightUpT, WheelRightDownT, WheelLeftUpT, WheelLeftDownT;

    void updateWheelPose(WheelCollider _col, Transform _trans)
    {
        Vector3 getTrans = _trans.position;
        Quaternion getRot = _trans.rotation;

        
        _col.GetWorldPose(out getTrans, out getRot);

        _trans.position = getTrans;
        _trans.rotation = getRot;
    }

    void updateAllWheelsPos()
    {
        updateWheelPose(WheelRightUp, WheelRightUpT);
        updateWheelPose(WheelLeftUp, WheelLeftUpT);
        updateWheelPose(WheelLeftDown, WheelLeftDownT);
        updateWheelPose(WheelRightDown, WheelRightDownT);
    }

	// Update is called once per frame
	void FixedUpdate () {
        float forward = Input.GetAxisRaw("Vertical") * speed;
        float steer = Input.GetAxisRaw("Horizontal") * steerSpeed;

        WheelRightUp.motorTorque = forward;
        WheelLeftUp.motorTorque = forward;

        WheelLeftUp.steerAngle = steer;
        WheelRightUp.steerAngle = steer;

        //updateAllWheelsPos();
    }
}
