﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowCar : MonoBehaviour {

    public Transform target;
    public float lookSpeed;
    public float followSpeed;
    public Vector3 offset;

    public void lookAtTarget()
    {
        Vector3 lookDir = target.transform.position - transform.position;

        Quaternion rot = Quaternion.LookRotation(lookDir, Vector3.up);
        transform.rotation = Quaternion.Lerp(transform.rotation, rot, lookSpeed * Time.deltaTime);
    }

    public void moveToTarget()
    {
        Vector3 targetPos = target.position + target.forward * offset.z + target.up * offset.y + target.right * offset.x;
        transform.position = Vector3.Lerp(transform.position, targetPos, followSpeed * Time.deltaTime);
    }

    private void FixedUpdate()
    {
        lookAtTarget();
        moveToTarget();
    }
}
