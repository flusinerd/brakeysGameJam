﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class drawPath : MonoBehaviour {
    public Color lineColor;
    public bool drawLines = false;

    private List<Transform> wayPoints = new List<Transform>();

    //Function is called when Scene View is updated
    private void OnDrawGizmos()
    {
        Gizmos.color = lineColor;

        Transform[] waypointTransforms = GetComponentsInChildren<Transform>();
        wayPoints = new List<Transform>();
        for (int i = 0; i< waypointTransforms.Length; i++)
        {
            //Check if not the ownTransform
            if (waypointTransforms[i] != transform)
            {
                //Add to List
                wayPoints.Add(waypointTransforms[i]);
            }
        }

        for (int i=0; i<wayPoints.Count; i++)
        {
            Vector3 currentWaypoint = wayPoints[i].position;
            Vector3 previousWaypoint = Vector3.zero;
            if (i > 0)
            {
                previousWaypoint = wayPoints[i - 1].position;
            }
            else if (i == 0 && wayPoints.Count > 1)
            {
                previousWaypoint = wayPoints[wayPoints.Count - 1].position;
            }
            if (drawLines == true) { 
                Debug.DrawLine(previousWaypoint, currentWaypoint);
            }
        }
    }
}
