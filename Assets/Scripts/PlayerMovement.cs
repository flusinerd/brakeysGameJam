﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public Rigidbody player;
    private float speed = 20f;
    private float steerSpeed = 5f;

	void FixedUpdate ()
    {
        float steer = Input.GetAxisRaw("Horizontal") * steerSpeed;

        player.AddForce(-transform.right * speed);
        player.AddTorque(new Vector3(0, steer, 0), ForceMode.Force);
	}

}
