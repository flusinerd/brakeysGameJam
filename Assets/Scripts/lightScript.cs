﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lightScript : MonoBehaviour
{

    Ray ray;
    RaycastHit hit;
    Vector2 mousePos;

    public Camera cam;

    bool countdownDone = false;

    void Update()
    {
        // find mouse position in 3D game world
        ray = cam.ScreenPointToRay(Input.mousePosition);

        // test if raycast hit a car
        if (Physics.Raycast(ray, out hit) && hit.transform.tag == "Car")
        {
            // timer
            if (countdownDone == false)
            {
                StartCoroutine("timer");
            }
            else
            {
                // timer done, crash
                crash(hit.transform);
            }
            // stop timer due to cursor moving off
        }
        else
        {
            StopCoroutine("timer");
        }

    }

    // timer
    IEnumerator timer()
    {
        for (int i = 3; i > 0; i--)
        {
            Debug.Log(i);
            yield return new WaitForSeconds(1);
        }
        countdownDone = true;
    }

    // crash
    void crash(Transform car)
    {
        Debug.Log(car.name);
        countdownDone = false;
    }
}